const dbConnection = require('../config/mongoConnection')
const data = require('../data')

async function main() {
  const db = await dbConnection()
  await db.dropDatabase()

  await data.wheel.create({
    slug: 'fdroxc',
    overdue_enabled: true,
    spin_or_shift: 'spin',
    roommates: ['James', 'Charles', 'Cameron'],
    jobs: [
      {
        name: "Clean bathroom floor",
        description: "Clean bathroom floor. 1x per week"
      },
      {
        name: "Clean surfaces",
        description: "Clean all the surfaces in the kitchen (countertop, mobile" +
          "counter, table). 1x per week"
      },
      {
        name: "Take out trash",
        description: "Take the trash out. 2x per week."
      },
    ]
  })

  await data.wheel.create({
    title: 'Single Chore',
    overdue_enabled: true,
    spin_or_shift: 'shift',
    roommates: ['James', 'Charles', 'Cameron'],
    jobs: [
      {
        name: "Do dishes, run dishwasher",
        description: "Run the dishwasher, and empty it the next day."
      },
    ]
  })
  
  await db.serverConfig.close()
}

main()
