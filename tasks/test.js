const dbConnection = require('../config/mongoConnection')
const data = require('../data')
const { ObjectId } = require('mongodb')

async function main() {
  const db = await dbConnection()
  await db.dropDatabase()

  /**
   * Wheel Creation
   **/

  try {
    await data.wheel.create({
      slug: 'fdroxc',
      overdue_enabled: true,
      spin_or_shift: 'spin',
      roommates: ['James', 'Charles', 'Cameron'],
      jobs: [
        {
          name: "Clean bathroom floor",
          description: "Clean bathroom floor. 1x per week"
        },
        {
          name: "Clean surfaces",
          description: "Clean all the surfaces in the kitchen (countertop, mobile" +
            "counter, table). 1x per week"
        },
        {
          name: "Take out trash",
          description: "Take the trash out. 2x per week."
        },
      ]
    })
  } catch (e) { console.log(`Test Failed! Reason: ${e}`) }

  try {
    await data.wheel.create({
      title: 'Single Chore',
      overdue_enabled: true,
      spin_or_shift: 'shift',
      roommates: ['James', 'Charles', 'Cameron'],
      jobs: [
        {
          name: "Do dishes, run dishwasher",
          description: "Run the dishwasher, and empty it the next day."
        },
      ]
    })
  } catch (e) { console.log(`Test Failed! Reason: ${e}`) }


  /**
   * Invalid Wheel Creation
   **/

  try { // already existing slug
    await data.wheel.create({
      slug: 'fdroxc',
      title: 'already existing slug',
      overdue_enabled: true,
      spin_or_shift: 'spin',
      roommates: ['James', 'Charles', 'Cameron'],
      jobs: [
        {
          name: "Clean bathroom floor",
          description: "Clean bathroom floor. 1x per week"
        },
        {
          name: "Clean surfaces",
          description: "Clean all the surfaces in the kitchen (countertop, mobile" +
            "counter, table). 1x per week"
        },
        {
          name: "Take out trash",
          description: "Take the trash out. 2x per week."
        },
      ]
    })
  } catch (e) { }
  
  try { // missing overdue_enabled
    await data.wheel.create({
      spin_or_shift: 'spin',
      title: 'Missing overdue_enabled',
      roommates: ['James', 'Charles', 'Cameron'],
      jobs: [
        {
          name: "Clean bathroom floor",
          description: "Clean bathroom floor. 1x per week"
        },
        {
          name: "Clean surfaces",
          description: "Clean all the surfaces in the kitchen (countertop, mobile" +
            "counter, table). 1x per week"
        },
        {
          name: "Take out trash",
          description: "Take the trash out. 2x per week."
        },
      ]
    })
  } catch (e) { }


  try { // boolean spin_or_shift
    await data.wheel.create({
      spin_or_shift: true,
      overdue_enabled: false,
      title: 'Boolean spin_or_shift',
      roommates: ['James', 'Charles', 'Cameron'],
      jobs: [
        {
          name: "Clean bathroom floor",
          description: "Clean bathroom floor. 1x per week"
        },
        {
          name: "Clean surfaces",
          description: "Clean all the surfaces in the kitchen (countertop, mobile" +
            "counter, table). 1x per week"
        },
        {
          name: "Take out trash",
          description: "Take the trash out. 2x per week."
        },
      ]
    })
  } catch (e) { }

  try { // Missing roommates
    await data.wheel.create({
      spin_or_shift: 'spin',
      overdue_enabled: false,
      title: 'Missing roommates',
      jobs: [
        {
          name: "Clean bathroom floor",
          description: "Clean bathroom floor. 1x per week"
        },
        {
          name: "Clean surfaces",
          description: "Clean all the surfaces in the kitchen (countertop, mobile" +
            "counter, table). 1x per week"
        },
        {
          name: "Take out trash",
          description: "Take the trash out. 2x per week."
        },
      ]
    })
  } catch (e) { }


  try { // One of the jobs doesn't have a name
    await data.wheel.create({
      spin_or_shift: 'spin',
      overdue_enabled: false,
      title: 'Job missing name',
      roommates: ['James', 'Charles', 'Cameron'],
      jobs: [
        {
          description: "Clean bathroom floor. 1x per week"
        },
        {
          name: "Clean surfaces",
          description: "Clean all the surfaces in the kitchen (countertop, mobile" +
            "counter, table). 1x per week"
        },
        {
          name: "Take out trash",
          description: "Take the trash out. 2x per week."
        },
      ]
    })
  } catch (e) { }

  /**
   * Wheel Get
   **/

  try {
    let w = await data.wheel.get.bySlug('fdroxc')
    console.log(w)
  } catch (e) { console.log(`Test Failed! Reason: ${e}`) }

  try { // slug doesn't exist
    let w = await data.wheel.get.bySlug('fdjsakfads')
    console.log(w)
  } catch (e) { }

  try { // id doesn't exist
    let w = await data.wheel.get.byId(new ObjectId())
    console.log(w)
  } catch (e) { }

  try { // slug: no parameter
    let w = await data.wheel.get.bySlug()
    console.log(w)
  } catch (e) { }

  try { // id: no parameter
    let w = await data.wheel.get.byId()
    console.log(w)
  } catch (e) { }
  
  /**
   * Wheel Spin
   **/

  // TODO

  /**
   * Wheel Shift
   **/

  // TODO

  await db.serverConfig.close()
}

main()
