---
title: Docs
---

[[_TOC_]]

# Data

ATM, we'll only have one collection - the Wheels collection.

The Wheels collection will be a collection of documents representing the users'
chore wheels. Each document will have the following properties:

| Property               | Description                                                                    | Example                            |
|------------------------|--------------------------------------------------------------------------------|------------------------------------|
| `_id`                  | Mongo ObjectID                                                                 |                                    |
| `title`                | A short title for the wheel (optional)                                         |                                    |
| `slug`                 | A short unique ID for this chore wheel                                         | `xfslsr`                           |
| `roommates`            | A list of the roommates involved.                                              | `['James', 'Charles', 'Cameron' ]` |
| `pin`                  | A hash of a 4 digit pin number password for reshuffling and editing (optional) |                                    |
| `overdue_enabled`      | True if Overdue Rules are enabled                                              |                                    |
| `spin_or_shift`        | Controls whether the wheel spins and randomly picks new jobs, or simply shifs  | `'spin'` or `'shift'`              |
| `state`                | An object pairing roommates' names to their jobs                               |                                    |
| `state.<name>`         | A list of job states that this user has.                                       |                                    |
| `state.<name>.job`     | The ObjectId of the job this user is doing                                     |                                    |
| `state.<name>.overdue` | True if this job is overdue                                                    |                                    |
| `jobs`                 | A list of objects representing the individual chores                           |                                    |
| `jobs.name`            | The name of the job                                                            | `'Clean bathroom floor'`           |
| `jobs.description`     | A description of the job (optional)                                            |                                    |
| `jobs._id`             | Mongo ObjectId for this job                                                    |                                    |

Here's an example object:

```js
let o = {
  _id: '6116b882cac3f72538b8e4e5',
  title: 'Chores',
  slug: 'fdroxc',
  overdue_enabled: true,
  spin_or_shift: 'spin',
  roommates: ['James', 'Charles', 'Cameron'],
  jobs: [
    {
      _id: '6116b75bcac3f72538b8e4e2',
      name: "Clean bathroom floor",
      description: "Clean bathroom floor. 1x per week"
    },
    {
      _id: '6116b75dcac3f72538b8e4e3',
      name: "Clean surfaces",
      description: "Clean all the surfaces in the kitchen (countertop, mobile" +
        "counter, table). 1x per week"
    },
    {
      _id: '6116b75bcac3f72538b8e4e4',
      name: "Take out trash",
      description: "Take the trash out. 2x per week."
    },
  ],
  state: {
    James: [{
      job: '6116b75bcac3f72538b8e4e2',
      overdue: false
    }],
    Charles: [{
      job:'6116b75bcac3f72538b8e4e3',
      overdue: false
    }],
    Cameron: [{
      job:'6116b75bcac3f72538b8e4e4',
      overdue: true
    }]
  }
}
```

Data-wise, different states of the wheel are represented simply as different
pairings of roommates to jobs in the `state` wheel. 

The `overdue` key will be used when re-spinning with Overdue Rules enabled. If
there are overdue jobs in the state, instead of being re-shuffled and given to
new people, these jobs will stick with the person who let them go overdue.

The `spin_or_shift` key will be used to determine whether or not the wheel
should spin freely and randomly assign jobs, or if it should just shift once,
and go down the line one by one.

# Routes

## Page Routes

### GET `/` - Main Page

Shows a form that the user can use to create a new chore wheel.

### GET `/:slug` - Wheel View

Shows the chore wheel whose slug is `:slug`. 

If this chore wheel has no `pin`, this page also shows an edit button, and a
spin button to change the wheel's state. 

If this chore wheel *does* have a `pin`, this page simply displays the chore
wheel, and has a textbox at the bottom where the user can enter their pin. Once
entered, the page shows edit and spin as options again.

If there are overdue chores, instead of being displayed on the wheel, they'll
be displayed floating next to the person's name. 

## `/api` - API Routes

### POST `/api/wheel` - Create a new chore wheel

Create a new chore wheel. The body expects JSON data in the following format:

| Property           | Description                                                                   | Example                            |
|--------------------|-------------------------------------------------------------------------------|------------------------------------|
| `slug`             | A short unique ID for this chore wheel (optional)                             | `xfslsr`                           |
| `title`            | A short title for the wheel (optional)                                        |                                    |
| `roommates`        | A list of the roommates involved. Each name must be unique                    | `['James', 'Charles', 'Cameron' ]` |
| `pin`              | A 4 digit pin number password for reshuffling and editing (optional)          |                                    |
| `overdue_enabled`  | True if overdue rules are enabled.                                            |                                    |
| `spin_or_shift`    | Controls whether the wheel spins and randomly picks new jobs, or simply shifs | `'spin'` or `'shift'`              |
| `jobs`             | A list of objects representing the individual chores                          |                                    |
| `jobs.name`        | The name of the job                                                           | `'Clean bathroom floor'`           |
| `jobs.description` | A description of the job (optional)                                           |                                    |

Anything labeled optional in this table can be omitted in the post data or left
as null/undefined. 

In the post data, if provided, `pin` should be the plaintext pin number that the
user chose. It should be only digits, and once provided, it should be stored as
a hash, not in plaintext.

If omitted, the `slug` should be autogenerated. 

### GET `/api/wheel/:id` - Get a chore wheel by its ID

Pretty self explanatory - return the chore wheel by its ID. 

The only caveat is that the `pin`, if provided, should be removed from the
returned data.

### POST `/api/wheel/:id/spin` - Spin the wheel!

Spins the wheel and re-assigns the roommates with their jobs. 

The re-shuffling should happen linearly, where the order of roommates and the
order of jobs in the `roommates` and `jobs` lists should be kept intact. 

If this wheel has a `pin` field, the body expects a `pin` field as well.

If everything is successful, the new `state` field should be returned. 

### POST `/api/wheel/:id/shift` - Shift the wheel by one!

Spins the wheel and re-assigns the roommates with their jobs, but in a way that
it only moves the wheel by one.

The shifting should happen linearly, where the order of roommates and the order
of jobs in the `roommates` and `jobs` lists should be kept intact.

If this wheel has a `pin` field, the body expects a `pin` field as well.

If everything is successful, the new `state` field should be returned. 

### Modification

TODO: Routes that modify the chore wheel are coming soon! 
