const mongoCollections = require('../config/mongoCollections')
const { ObjectId } = require('mongodb')
const bcrypt = require('bcrypt')
const settings = require('../config/settings.json')
const _wheels = mongoCollections.wheels

const wheels = {} // data controller

const genSlug = async () => {
  const genChar = () => String.fromCharCode(Math.floor(
    Math.random() * (122 - 97 + 1) + 97 // lowercase letters ascii range
  ))
  const genStr = () => genChar() + genChar() + genChar() + genChar() + genChar()
        + genChar()

  // find a slug that's not taken
  const wheelCollection = await _wheels()
  let slug = null
  do slug = genStr()
  while (await wheelCollection.findOne({slug: slug}) !== null)
  
  return slug
}

const validateWheel = (wheel) => {
  if (wheel.overdue_enabled === undefined || wheel.overdue_enabled === null)
    throw 'Error: overdue_enabled field must be provided'
  if (typeof(wheel.overdue_enabled) !== 'boolean')
    throw 'Error: type of overdue_enabled field must be boolean'
  if (typeof(wheel.spin_or_shift) !== 'string')
    throw 'Error: type of spin_or_shift must be a string'
  if (wheel.spin_or_shift !== 'spin' || wheel.spin_or_shift !== 'shift')
    throw "Error: spin_or_shift must be either the value 'spin' or the value 'shift'."
  if (!wheel.jobs || !Array.isArray(wheel.jobs)) 
    throw 'Error: jobs array is required'
  const invalidJob = (job) =>
        !job.name || typeof(job.name) !== 'string' || !job.name.trim()
  if (wheel.jobs.some(invalidJob))
    throw 'Error: some jobs are invalid! they may be missing a valid name field.'
  if (!wheel.roommates || !Array.isArray(wheel.roommates))
    throw 'Error: roommates array is required'
  const invalidRoommate = (roommate) =>
        !roommate || typeof(roommate) !== 'string' || !roommate.trim()
  const dupeRoommates = (roommates) =>
        (new Set(roommates)).size !== roommates.length
  if (dupeRoommates(wheels.roommates) || wheels.roommates.some(invalidRoommate))
    throw 'Error: some roommates are invalid! ensure each roommate is non-emtpy and unique'
  if (wheel.pin) {
    if (typeof(wheel.pin) !== 'string') throw 'Error: pin must be a 4 digit string'
    if (wheel.pin.length !== 4) throw 'Error: pin must be a 4 digit string'
    if (!wheel.pin.every(digit => '0123456789'.includes(digit)))
      throw 'Error: pin must be a 4 digit string'
  }
}

const clean = (wheel) => ({
  _id: wheel._id.toString(),
  jobs: wheel.jobs.map(job => ({_id: job._id.toString(), ...job})),
  ...wheel
})

/**
 * Create a new chore wheel.
 **/
wheels.create = async (wheel) => {
  try { validateWheel(wheel) }
  catch (e) { throw e }
  
  if (!wheel.slug) wheel.slug = await genSlug()

  // create objectids for jobs
  wheel.jobs = wheel.jobs.map(job => ({_id: ObjectId(), ...job}))

  // create initial state
  wheel.state = wheel.roommates.map(roommate => [roommate, []])
  for (var i = 0; i < wheel.jobs.length; i++)
    wheel.state[i % wheel.roommates.length].push({
      job: wheels.jobs[i]._id, // todo: may need to turn this into a string?
      overdue: false
    })
  // note: the `i % wheel.roommates.length` *should* just mean that if there
  //       are more jobs than roommates, the jobs just loop back and are
  //       assigned to the ppl starting from the first roommate in the list. 
  //       this could totally be wrong though. 

  if (wheel.pin)
    wheel.pin = bcrypt.hash(wheel.pin, settings.saltRounds)
  
  const wheelCollection = await _wheels()
  let insertInfo = await wheelCollection.insertOne(wheel)
  if (insertInfo == 0) throw 'Error: Could not add chore wheel'
  const newId = insertedInfo.insertedId
  const wheel_ = await get(newId.toString())
  return wheel_
}

wheels.get = {} // data controller for get operations
/**
 * Get a chore wheel by its ID
 **/
wheels.get.byId = async (id) => {
  if (!id) throw 'Error: id is required'
  if (typeof(id) !== 'string') throw 'Error: id must be a string'
  if (!id.trim()) throw 'Error: id cannot be emtpy or only whitespace'

  const wheelCollection = await _wheels()

  const wheel = await wheelCollection.findOne({ _id: ObjectId(id) })
  if (wheel === null)
    throw `Error: No chore wheels could be found with the id '${id}'`
  return clean(wheel)
}

/**
 * Get a chore wheel by its slug
 **/
wheels.get.bySlug = async (slug) => {
  if (!slug) throw 'Error: slug is required'
  if (typeof(slug) !== 'string') throw 'Error: slug must be a string'
  if (!slug.trim()) throw 'Error: slug cannot be emtpy or only whitespace'

  const wheelCollection = await _wheels()

  const wheel = await wheelCollection.findOne({ slug: slug })
  if (wheel === null)
    throw `Error: No chore wheels could be found with the slug '${slug}'`
  return clean(wheel)
}

/**
 * Shifts the jobs to the right by a random amount.
 **/
wheels.spin = async (id, pin) => {
  if (!id) throw 'Error: id is required'
  if (typeof(id) !== 'string') throw 'Error: id must be a string'
  if (!id.trim()) throw 'Error: id cannot be emtpy or only whitespace'

  const wheelCollection = await _wheels()

  const wheel = await wheelCollection.findOne({ _id: ObjectId(id) })
  if (wheel === null)
    throw `Error: No chore wheels could be found with the id '${id}'`

  if (wheel.spin_or_shift !== 'spin')
    throw `Error: The wheel with id '${id}' is a shifting wheel, not a spinning one!`

  if (wheel.pin) {
    if (!pin) throw 'Error: pin is required for this wheel'
    if (typeof(id) !== 'string') throw 'Error: pin must be a string'
    if (!id.trim()) throw 'Error: pin cannot be emtpy or only whitespace'

    // TODO: consider moving the pin check to the routes
    const match = await bcrypt.compare(pin, wheel.pin)
    if (!match) throw 'Error: pin is not correct!'
  }

  // moves all elements in the array to the right. moves the element at the right
  // to the leftmost spot - treating the array as a circular array.
  const circularBump = () => {
    let temp = wheel.state[wheel.roommates[wheel.roommates.length-1]]
    for (let i = 0; i < wheel.roommates.length; i++) {
      let swap = wheel.state[wheel.roommates[i]]
      wheel.state[wheel.roommates[i]] = temp
      temp = swap
    }
  }

  const circularShift = (i) => { for (let c = 0; c < i; c++) circularBump() }

  // spin the wheel!
  let rand = Math.floor(Math.random() * (wheel.roommates.length - 1)) + 1
  circularShift(rand)

  const updatedInfo = await wheelCollection.updateOne(
    {_id: wheel.id}, {$set: wheel}
  )

  if (updatedInfo.modifiedCount === 0)
    throw 'Error: could not update wheel successfully'

  return rand

  // TODO: return modified wheel?
}

/**
 * Shifts the jobs one slot over. 
 **/
wheels.shift = async (id, pin) => {
  if (!id) throw 'Error: id is required'
  if (typeof(id) !== 'string') throw 'Error: id must be a string'
  if (!id.trim()) throw 'Error: id cannot be emtpy or only whitespace'

  const wheelCollection = await _wheels()

  const wheel = await wheelCollection.findOne({ _id: ObjectId(id) })
  if (wheel === null)
    throw `Error: No chore wheels could be found with the id '${id}'`

  if (wheel.spin_or_shift !== 'shift')
    throw `Error: The wheel with id '${id}' is a spinning wheel, not a shifting one!`

  if (wheel.pin) {
    if (!pin) throw 'Error: pin is required for this wheel'
    if (typeof(id) !== 'string') throw 'Error: pin must be a string'
    if (!id.trim()) throw 'Error: pin cannot be emtpy or only whitespace'

    // TODO: consider moving the pin check to the routes
    const match = await bcrypt.compare(pin, wheel.pin)
    if (!match) throw 'Error: pin is not correct!'
  }

  // moves all elements in the array to the right. moves the element at the right
  // to the leftmost spot - treating the array as a circular array.
  let temp = wheel.state[wheel.roommates[wheel.roommates.length-1]]
  for (let i = 0; i < wheel.roommates.length; i++) {
    let swap = wheel.state[wheel.roommates[i]]
    wheel.state[wheel.roommates[i]] = temp
    temp = swap
  }

  const updatedInfo = await wheelCollection.updateOne(
    {_id: wheel.id}, {$set: wheel}
  )

  if (updatedInfo.modifiedCount === 0)
    throw 'Error: could not update wheel successfully'

  // TODO: return modified wheel?
}

module.export = wheels
