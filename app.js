const express = require("express"),
      app = express(),
      configRoutes = require("./routes"),
      exphbs = require("express-handlebars"),
      data = require("./data")

app.engine("handlebars", exphbs({defaultLayout: "main"}))
app.set("view engine", "handlebars")

configRoutes(app)

app.listen(3000, () => {
  console.log("Server running at http://localhost:3000");
})
